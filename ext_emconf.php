<?php

$EM_CONF['async_image_processing'] = [
    'title' => 'Async image processing',
    'description' => 'Adds asynchronous image processing to TYPO3.',
    'category' => 'example',
    'constraints' => [
        'depends' => [
            'typo3' => '11.0.0-dev',
        ],
        'conflicts' => [],
    ],
    'state' => 'experimental',
    'uploadfolder' => 0,
    'createDirs' => '',
    'author' => 'Tim Schreiner',
    'author_email' => 'timn@neues.studio',
    'author_company' => 'Neues Studio',
    'version' => '0.0.1',
];
