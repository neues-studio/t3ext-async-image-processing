<?php

$GLOBALS['TYPO3_CONF_VARS']['SYS']['fal']['processors']['DeferredLocalFrontendImageProcessor'] = [
    'className' => \NeuesStudio\AsyncImageProcessing\Resource\Processing\DeferredLocalFrontendImageProcessor::class,
    'before' => [
        'LocalImageProcessor',
    ],
];
