# TYPO3 Async image processing

**This is an experimental extension that will not be developed actively!**

This extension is build for working with TYPO3 master branch.

To make it work, use 
`Resources/Private/FolderStructureTemplateFiles/root-htaccess`.
This htaccess allows files inside fileadmin folder to put through
further processing by sending request to index.php.

The image dimension class is related to 
https://review.typo3.org/c/Packages/TYPO3.CMS/+/65237
  