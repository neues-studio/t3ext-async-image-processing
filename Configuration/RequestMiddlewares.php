<?php

return [
    'frontend' => [
        'neues-studio/async-image-processing/deferred-local-frontend-image-processing' => [
            'target' => \NeuesStudio\AsyncImageProcessing\Middleware\DeferredLocalFrontendImageProcessing::class,
            'after' => [
                'typo3/cms-frontend/maintenance-mode',
            ],
            'before' => [
                'typo3/cms-frontend/site',
            ],
        ],
    ],
];
