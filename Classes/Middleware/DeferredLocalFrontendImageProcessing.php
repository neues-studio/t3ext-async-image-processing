<?php

declare(strict_types=1);

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace NeuesStudio\AsyncImageProcessing\Middleware;

use NeuesStudio\AsyncImageProcessing\Resource\ProcessedFileRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class DeferredLocalFrontendImageProcessing implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($this->isImageRequest($request) && $this->isUnprocessedFile($request)) {
            $processedFile = $this->processFile($request);

            return $processedFile->getStorage()->streamFile($processedFile);
        }

        return $handler->handle($request);
    }

    protected function isImageRequest(ServerRequestInterface $request): bool
    {
        $targetFile = (string)$request->getRequestTarget();
        $requestedFileExtension = pathinfo($targetFile, PATHINFO_EXTENSION);

        return GeneralUtility::inList(strtolower($GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'] ?? ''), $requestedFileExtension);
    }

    protected function isUnprocessedFile(ServerRequestInterface $request): bool
    {
        $targetFile = (string)$request->getRequestTarget();
        $targetFile = ltrim($targetFile, '/');
        $resourceFactory = GeneralUtility::makeInstance(ResourceFactory::class);
        $storage = $resourceFactory->getStorageObject(0, [], $targetFile);

        return $storage->getUid() > 0
            && $storage->isWritable()
            && $storage->getDriverType() == 'Local'
            && !$storage->hasFile('/' . $targetFile);
    }

    protected function processFile($request): ?ProcessedFile
    {
        $targetFile = (string)$request->getRequestTarget();
        $targetFile = ltrim($targetFile, '/');
        $resourceFactory = GeneralUtility::makeInstance(ResourceFactory::class);
        $processedFileRepository = GeneralUtility::makeInstance(ProcessedFileRepository::class);
        $storage = $resourceFactory->getStorageObject(0, [], $targetFile);
        $processedFile = $processedFileRepository->findByStorageAndIdentifier($storage, '/' . $targetFile, true);

        $originalFile = $processedFile->getOriginalFile();
        $processingConfiguration = $processedFile->getProcessingConfiguration();
        $taskType = $processedFile->getTaskIdentifier();

        $processedFile = $originalFile->process($taskType, $processingConfiguration);

        return $processedFile;
    }
}
